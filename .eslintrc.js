module.exports = {
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint'],
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
    ],
    rules: {
        "camelcase": ["error", {"properties": "always"}],
        "quotes": ["error", "single"],
        "semi": ["error", "always"]
    }
}