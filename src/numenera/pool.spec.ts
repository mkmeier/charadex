import {Pool} from './pool';

describe('pool', ()=> {
    describe('constructor', ()=> {
        it('should default to full with zero edge', () => {
            const pool = new Pool(5);

            expect(pool.size).toEqual(5);
            expect(pool.value).toEqual(5);
            expect(pool.edge).toEqual(0);
        });

        it('should override default edge when provided', () => {
            const pool = new Pool(3, 2);
            expect(pool.edge).toEqual(2);
        });

        it('should override the value value when provided', () => {
            const pool = new Pool(6, 1, 3);
            expect(pool.value).toEqual(3);
        });

        it('should set value to size if provided value is over size', () => {
            const pool = new Pool(4, 0, 7);
            expect(pool.value).toEqual(pool.size);
        });

        it('should truncate to int values if floats are provided', () => {
            const pool = new Pool(4.5, 2.2, 3.8);
            expect(pool.size).toEqual(4);
            expect(pool.value).toEqual(3);
            expect(pool.edge).toEqual(2);
        });
    });

    describe('spend', () => {
        it('should reduce value by the amount of spend', () => {
            const pool = new Pool(5);
            pool.spend(3);

            expect(pool.value).toEqual(2);
        });

        it('should buffer the amount removed from value by the edge', () => {
            const pool = new Pool(5, 2);

            pool.spend(3);
            expect(pool.value).toEqual(4);
        });

        it('should not reduce the value if spend amount is less than the edge', () => {
            const pool = new Pool(10, 5);

            pool.spend(3);

            expect(pool.value).toEqual(10);
        });

        it('should return an error if spend would reduce the value below zero', () => {
            const pool = new Pool(3, 1);

            expect(() => pool.spend(5)).toThrow('Insufficient value');
        });
    });

    describe('reduce', () => {
        it('should reduce value by the amount', () => {
            const pool = new Pool(5);
            pool.reduce(3);
            expect(pool.value).toEqual(2);
        });

        it('should zero out the pool when amount is greater than value', () => {
            const pool = new Pool(5);
            pool.reduce(10);
            expect(pool.value).toEqual(0);
        });

        it('should return the amount remaining after reduction', () => {
            const pool = new Pool(5);

            expect(pool.reduce(3)).toEqual(0);
            expect(pool.reduce(3)).toEqual(1);
        });
    });

    describe('restore', () => {
        it('should restore the pool by the specified amount', () => {
            const pool = new Pool(10, 0, 3);
            pool.restore(4);
            expect(pool.value).toEqual(7);
        });

        it('should return the unused amount', () => {
            const pool = new Pool(10, 0, 3);
            expect(pool.restore(4)).toEqual(0);
            expect(pool.restore(4)).toEqual(1);
        });
    });

    describe('reset', () => {
        it('should reset the value to the size', () => {
            const pool = new Pool(5, 0, 2);
            pool.reset();
            expect(pool.value).toEqual(pool.size);
        });

        it('should return the amount used to refill the pool', () => {
            const pool = new Pool(5, 0, 2);

            expect(pool.reset()).toEqual(3);
        });
    });

    describe('increaseSize', () => {
        it('should increase the size of the pool', () => {
            const pool = new Pool(3);
            pool.increaseSize(3);
            expect(pool.size).toEqual(6);
        });

        it('should keep the pool full if it was full when increased', () => {
            const pool = new Pool(5);
            pool.increaseSize(3);
            expect(pool.isFull()).toEqual(true);
        });

        it('should not change the value of the pool when not full', () => {
            const pool = new Pool(10, 0, 2);
            pool.increaseSize(4);
            expect(pool.value).toEqual(2);
        });

        it('should return the new size of the pool', () => {
            const pool = new Pool(4);
            expect(pool.increaseSize(2)).toEqual(6);
        });
    });

    describe('increaseEdge', () => {
        it('should increase the edge', () => {
            const pool = new Pool(4);
            pool.increaseEdge(2);
            expect(pool.edge).toEqual(2);
        });

        it('should return the new edge', () =>{
            const pool = new Pool(4, 3);
            expect(pool.increaseEdge(2)).toEqual(5);
        });
    });

    it('should be full when created', () => {
        const pool = new Pool(3);
        expect(pool.isFull()).toEqual(true);
    });

    it('should be empty with value is 0', () => {
        const pool = new Pool(3, 0, 0);
        expect(pool.isEmpty()).toEqual(true);
    });
});
