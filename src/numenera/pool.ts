/**
 * Represents an ability pool. It tracks the max size, current value, and edge.
 */
export class Pool {
    private _size: number;
    private _value: number;
    private _edge: number;

    constructor(size: number, edge = 0, current = size) {
        if (current > size) {
            current = size;
        }
        this._size = this.normalize(size)
        this._value = this.normalize(current);
        this._edge = this.normalize(edge);
    }

    isEmpty(): boolean {
        return this._value == 0;
    }

    isFull(): boolean {
        return this._value == this._size;
    }

    /**
     * Spend a specific amount from the current value. This is used when spending pool such as using effort or
     * activating abilities. This is because spending points in this pool takes edge into account.
     *
     * @param {number} amount - The amount to spend.
     * @throws {Error} Throws an error if the amount to spend exceeds the current value plus the edge value.
     * @returns {void}
     */
    spend(amount: number): void {
        const toSpend = this.normalize(amount);

        if (toSpend > this.value + this.edge) {
            throw new Error('Insufficient value');
        }

        const toReduce = Math.max(0, toSpend - this.edge);

        this.value -= toReduce;
    }


    /**
     * Reduces the value by a specified amount. This reduction ignores effort. It's intended used is when the pool
     * is taking damage. If the amount to reduce would reduce the pool value below zero, then the amount yet to be
     * reduced is returned.
     *
     * @param {number} amount - The amount to reduce the value by.
     * @return {number} - The amount exceeding what can be reduced in this pool.
     */
    reduce(amount: number): number {
        const toReduce = this.normalize(amount);
        const previousValue = this._value;
        this.value = Math.max(0, this.value - toReduce);
        return Math.max(0, toReduce - previousValue);
    }

    /**
     * Restores the value of the pool by a given amount.
     *
     * @param {number} amount - The amount to restore.
     * @return {number} - The amount that could not be restored due to exceeding the capacity.
     */
    restore(amount:number): number {
        const need = this.size - this.value;
        const toRestore = this.normalize(amount);
        this.value = Math.min(this.value + toRestore, this.size);
        return Math.max(0, toRestore - need);
    }

    /**
     * Resets the pool to full, and returns the amount used to refill the pool.
     *
     * @returns {number} The amount restored to the pool.
     */
    reset(): number {
        const short = this.size - this.value;
        this.value = this.size;
        return short;
    }

    /**
     * Increases the size of the pool by the specified amount.
     * If the pool was full when increased, it will still be full.
     *
     * @param {number} amount - The amount by which to increase the size.
     * @return {number} - The new size of the pool.
     */
    increaseSize(amount: number): number {
        const toIncrease = this.normalize(amount);
        const wasFull = this.isFull();

        this._size += toIncrease;

        if (wasFull) {
            this.value = this.size;
        }

        return this._size;
    }

    /**
     * Increases the edge value by the specified amount.
     *
     * @param {number} amount - The amount by which to increase the edge value.
     * @return {number} - The updated edge value.
     */
    increaseEdge(amount: number): number {
        this.edge += this.normalize(amount);

        return this.edge;
    }

    private normalize(amount: number): number {
        return Math.max(0, Math.floor(amount));
    }

    // Properties

    get size(): number {
        return this._size;
    }

    set size(m: number) {
        this._size = Math.floor(m);
    }

    get value(): number {
        return this._value;
    }

    set value(c: number) {
        this._value = Math.floor(c);
    }

    get edge(): number {
        return this._edge;
    }

    set edge(e: number) {
        this._edge = Math.floor(e);
    }
}
