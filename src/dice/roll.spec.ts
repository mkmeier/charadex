import { rollDice } from './roll';
import { Die } from './die'; // Please replace 'your_die_class_filepath' with the actual path

describe('rollDice function', () => {
    let dieRollSpy: jest.SpyInstance;

    beforeEach(() => {
        // Spy on the Die class' roll function
        dieRollSpy = jest.spyOn(Die.prototype, 'roll');
    });

    afterEach(() => {
        // Restore the original functionality after each test
        dieRollSpy.mockRestore();
    });

    it('should correctly sum dice rolls and add modifier', () => {
        dieRollSpy.mockReturnValue(4); // Mock roll result
        const result = rollDice("2d6+3");
        expect(result).toBe(11); // 2 rolls of 4 each plus 3 modifier
    });

    it('should correctly sum dice rolls and subtract modifier', () => {
        dieRollSpy.mockReturnValue(5); // Mock roll result
        const result = rollDice("2d6-1");
        expect(result).toBe(9); // 2 rolls of 5 each minus 1 modifier
    });

    it('should correctly sum dice rolls without modifier', () => {
        dieRollSpy.mockReturnValue(3); // Mock roll result
        const result = rollDice("2d6");
        expect(result).toBe(6); // 2 rolls of 3 each
    });

    it('should throw an error for an invalid roll description', () => {
        // Invalid roll description should throw an error
        expect(() => rollDice("2b6")).toThrowError();
    });
});