import {Die} from "./die";

describe('Die', () => {
    const sidesArray = [2, 4, 6, 8, 10, 12, 20, 100];
    const tolerance = 0.05; // allow 10% deviation
    const rollsPerSide = 10000;

    sidesArray.forEach(sides => {
        it(`should roll each number approximately equally for a ${sides}-sided die`, () => {
            const rolls = rollsPerSide * sides;
            const die = new Die(sides);
            const results = new Array(sides).fill(0);

            // roll the die 'rolls' times and capture the results
            for (let i = 0; i < rolls; i++) {
                const roll = die.roll();
                results[roll - 1]++;
            }

            // expected number of times each side should be rolled
            const expected = rolls / sides;
            const lowerBound = expected * (1 - tolerance);
            const upperBound = expected * (1 + tolerance);

            // check each result in the results array
            results.forEach((result, index) => {
                expect(result).toBeGreaterThanOrEqual(lowerBound);
                expect(result).toBeLessThanOrEqual(upperBound);
            });
        });
    });
});


