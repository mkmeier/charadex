/**
 * Represents a die with a specified number of sides.
 */
export class Die {
    private readonly _sides: number

    constructor(sides: number) {
        if (sides < 1) {
            throw new Error("Number of sides must be positive")
        }

        this._sides = Math.floor(sides)
    }

    /**
     * Generates a random number between 1 and the number of sides.
     *
     * @returns {number} A random number between 1 and the number of sides.
     */
    roll(): number {
        return Math.floor(Math.random() * this._sides) + 1;
    }
}
