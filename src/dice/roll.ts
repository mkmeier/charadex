import {Die} from "./die";

/**
 * Rolls a set of dice and returns the total roll value.
 *
 * @param {string} rollDescription - The roll description in the format "NdM+X",
 *     where N is the number of dice, M is the number of sides on each die, and X is an optional modifier.
 *     Example: "2d6+3" means roll two six-sided dice and add 3 to the result.
 *
 * @return {number} The total roll value after rolling the dice and applying the modifier.
 */
export function rollDice(rollDescription: string): number {
    if (!isValidRollDescription(rollDescription)) {
        throw new Error(`Invalid roll description, check the format: '${rollDescription}'`)
    }

    // Split the roll description into main roll and optional modifier
    const parts = rollDescription.toLowerCase().split(/(\+|-)/).filter(Boolean);

    // Extract number of dice and number of sides from roll description
    const [diceNumber, sidesNumber] = parts[0].split('d').map(Number);

    // Parse the modifier, if it exists
    const modifier = parts[1] ? Number(parts.slice(1).join('')) : 0;

    let totalRolls = 0;

    // Roll each die and sum the rolls
    for(let i = 0; i < diceNumber; i++){
        const die = new Die(sidesNumber);
        totalRolls += die.roll();
    }

    // Apply the modifier
    totalRolls += modifier;

    return totalRolls;
}

/**
 * Regular expression to validate the format of a roll description.
 * The roll description should follow the format: [number of dice]d[number of sides][optional modifier].
 *
 * @type {RegExp}
 */
const rollDescriptionRegex: RegExp = /^[1-9]\d*d[1-9]\d*(([+\-])\d+)?$/i;

/**
 * Checks if a given roll description is valid.
 *
 * @param {string} description - The roll description to validate.
 * @return {boolean} Returns true if the roll description is valid, otherwise false.
 */
function isValidRollDescription(description: string): boolean {
    return rollDescriptionRegex.test(description);
}
